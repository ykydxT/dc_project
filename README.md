* **********************************************************************************
### PROJECT:     Analytics of Social Media Usage in the University of Melbourne
### DATE:        Semester 2 2017
### AUTHOR:      Dexiao Ye
### ID:          827499
### SUPSERVISOR: Prof Richard Sinnott
* **********************************************************************************

### STRUCTURE 

|----twitter_scraping-master: Timeline Scrapy (ref: https://github.com/bpb27/twitter_scraping)

|----pj_webserver: django framework for web server

|----harvester: harvest data from Twitter

|----Directed_network_ml: graph processing and machine learning(friend recommendation)

|----analysis: basic analytics(popular centers, center activities, popular users)
