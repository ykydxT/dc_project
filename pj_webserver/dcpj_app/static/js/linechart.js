var pydata = document.getElementById('data').innerHTML.toString().replace(/'/g, '"');
var myChart = echarts.init(document.getElementById('main'));
var centers = ["unimelb","uommedia","SciMelb","unilibrary","BusEcoNews","Government_UoM","electionwatch_","uomalumni","UniMelbMDHS", "FarragoMagazine"]
pydata = JSON.parse(pydata)
function singlecenter(centername) {
    var i = pydata.length;
    var result = [];
    while (i--){
        if(pydata[i][0] == centername) {
            result.push(pydata[i]);
        }
    }
    var dd = result.map(function (item){
       twoi = [new Date(item[1]), parseInt(item[2])]
       return twoi
    })
    return dd
}
console.log(singlecenter("unimelb"));
//var test = singlecenter("unimelb").map(function (item) {
//            twoi = [new Date(item[1]), 53];
//            return twoi;
//        });
//
//console.log(test);


myChart.setOption(option = {
    title: {
        text: 'Center Activities',
        y: 'top'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: centers,
        orient: 'vertical',
        x: "right"

    },
    xAxis: {
        type: "time",
        min:new Date('2017-01-01'),
        max:new Date('2017-11-01'),
        splitLine: {
            show: false
        }
    },
    yAxis: {
        splitLine: {
            show: false
        }
    },
    toolbox: {
        left: 'center',
        feature: {
            dataZoom: {
                yAxisIndex: 'none'
            },
            restore: {},
            saveAsImage: {}
        }
    },
    dataZoom: [{
        startValue: '2016-01-01'
    }, {
        type: 'inside'
    }],
//        visualMap: {
//            top: 10,
//            right: 10,
//            pieces: [{
//                gt: 0,
//                lte: 50,
//                color: '#096'
//            }, {
//                gt: 50,
//                lte: 100,
//                color: '#ffde33'
//            }, {
//                gt: 100,
//                lte: 150,
//                color: '#ff9933'
//            }, {
//                gt: 150,
//                lte: 200,
//                color: '#cc0033'
//            }, {
//                gt: 200,
//                lte: 300,
//                color: '#660099'
//            }, {
//                gt: 300,
//                color: '#7e0023'
//            }],
//            outOfRange: {
//                color: '#999'
//            }
//        },
    series: [{
        name: centers[0],
        type: 'line',
        data: singlecenter(centers[0])
        }
    ,{
        name: centers[1],
        type: 'line',
        data: singlecenter(centers[1])
    },{
        name: centers[2],
        type: 'line',
        data: singlecenter(centers[2])
    },{
        name: centers[3],
        type: 'line',
        data: singlecenter(centers[3])
    },{
        name: centers[4],
        type: 'line',
        data: singlecenter(centers[4])
    },{
        name: centers[5],
        type: 'line',
        data: singlecenter(centers[5])
    },{
        name: centers[6],
        type: 'line',
        data: singlecenter(centers[6])
    },{
        name: centers[7],
        type: 'line',
        data: singlecenter(centers[7])
    },{
        name: centers[8],
        type: 'line',
        data: singlecenter(centers[8])
    },{
        name: centers[9],
        type: 'line',
        data: singlecenter(centers[9])
    }]
    }
);
