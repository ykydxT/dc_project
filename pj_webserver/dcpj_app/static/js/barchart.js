var myChart = echarts.init(document.getElementById('main'));
var option = {
    title: {
        text: 'Top ten popular centers'
    },
    tooltip: {},
    legend: {
        data:['Number of followers'],
        x: 'right',
        padding: 10
    },
    xAxis: {
        data: ["unimelb","uommedia","SciMelb","unilibrary","BusEcoNews","Government_UoM","electionwatch_","uomalumni","UniMelbMDHS", "FarragoMagazine"],
        "type":"category",
        "axisLabel":{
            interval: 0
        }
    },

    yAxis: {},
    series: [{
        name: 'Number of followers',
        type: 'bar',
        data: [78088, 12808, 7326, 6815, 5937, 3998, 3949, 3908, 3624, 3274]
    }]
};

myChart.setOption(option);
