var pydata = document.getElementById('data_json').innerHTML.toString().replace(/'/g, '"');
pydata = JSON.parse(pydata)

function num_follower(i){
    var len = pydata[i].follower.length
    return len
}

function num_following(i){
    var len = pydata[i].following.length
    return len
}



function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        return b.indexOf(e) > -1;
    });
}

function get_nodes(i){
    var node = pydata[i];
    var result = [{category:0, name: node.name, symbolSize : 40}];
    var union = intersect(node.follower, node.following);
    for (k in node.follower){
        if (!union.includes(node.follower[k])){
            result.push({category: 1, name: node.follower[k], symbolSize: 40});
        }
    }
    for (j in node.following){
        if (!union.includes(node.following[j])){
            result.push({category: 2, name: node.following[j], symbolSize: 40});
        }
    }
    for (g in union){
        result.push({category: 3, name: union[g], symbolSize: 40});
    }
    return result;
}

function get_links(i){
    var node = pydata[i];
    var result = [];
    for (k in node.follower){
        result.push({source: node.follower[k], target: node.name});
    }
    for (j in node.following){
        result.push({source: node.name, target: node.following[j]});
    }
    return result;
}
console.log(get_nodes(0));
for (var i in pydata){
    var myChart = echarts.init(document.getElementById('user'+i));

    var option = {
        title: {
            text: parseInt(i) + 1 + ". " + pydata[i].name,
            subtext: 'Num of followers: '+ num_follower(i) + '\nNum of following: '+ num_following(i)
        },
        legend: {
            x: 'right',
            y: 'bottom',
            data:['follower','following', 'both']
        },
        nimationEasingUpdate: 'Linear',
        series: [{
        type: 'graph',
        layout: 'force',
        animation: false,
        draggable: true,
        edgeSymbol: ['', 'arrow'],
        focusNodeAdjacency : true,
        categories : [
                {
                    name: 'user'
                },                {
                    name: 'follower'
                },{
                    name: 'following'
                },{
                    name: 'both'
                }
        ],
        itemStyle: {
                normal: {
                    label: {
                        show: true,
                        textStyle: {
                            color: '#333'
                        }
                    },
                    nodeStyle : {
                        brushType : 'both',
                        borderColor : 'rgba(255,215,0,0.4)',
                        borderWidth : 1
                    }
                }
        },
        nodes: get_nodes(i),
        links: get_links(i),
        force: {
            repulsion: 1000,
            edgeLength: 200,
            repulsion: 100
        }
    }]

    }

    myChart.setOption(option);
}
