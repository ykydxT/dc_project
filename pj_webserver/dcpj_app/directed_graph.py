# this program is used to deal with network graph and perform machine learning
from igraph import *
import numpy as np
import itertools
from db_connection import couchdb_connect
import random
import pickle
import math
import time
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.externals import joblib


class directed_graph():
    def __init__(self):
        self.gp = Graph(directed=True)
        self.vdict = {}

    def get_gp(self):
        return self.gp

    def get_neighbors(self, v):
        return self.gp.neighbors(v)

    def edge_list(self):
        return self.gp.es

    # return num of edges in the graph
    def get_num_edges(self):
        print("The number of edges is:", len(self.gp.es))
        return len(self.gp.es)

    def in_neighbors(self, i):
        return self.gp.neighbors(i, mode=IN)

    def out_neighbors(self, j):
        return self.gp.neighbors(j, mode=OUT)

    def add_edge(self, vs, vt, w):
        ivs = 0
        ivt = 0
        if vs in self.vdict:
            ivs = self.vdict[vs]
        else:
            ivs = len(self.vdict)
            self.vdict[vs] = ivs
            self.gp.add_vertices(1)
        if vt in self.vdict:
            ivt = self.vdict[vt]
        else:
            ivt = len(self.vdict)
            self.vdict[vt] = ivt
            self.gp.add_vertices(1)
        self.gp.add_edge(ivs, ivt, weight=w)

    # load data from couchdb into igraph structure
    def load_graph(self, dbname):
        count = 0
        db = couchdb_connect(dbname)

        # retrieve data from couchdb
        for row in db.iterview('_all_docs', 10000, include_docs=True):
            count += 1
            print(count)
            try:
                self.add_edge(row.doc["from"], row.doc["to"], 1)
            except KeyError:
                self.add_edge(row.doc["user_id"], row.doc["center"], 1)

    # save the graph as static files
    def save_graph(self, fpath):
        self.gp.write_graphmlz(fpath + ".grp.gz", compresslevel=1)
        outf = open(fpath + ".igrp.pkl", "wb")
        pickle.dump(self.vdict, outf)
        outf.close()

    # read static graph files
    def read_graph(self, fpath):
        self.gp = Graph.Read_GraphMLz(fpath + ".grp.gz")
        outf = open(fpath + ".igrp.pkl", "rb")
        self.vdict = pickle.load(outf)
        print("finish reading")
        outf.close()

    # return index of a vertex
    def return_v(self, i):
        for j in self.vdict:
            if self.vdict[j] == i:
                return j

    # randomly generate negative examples
    def random_neg(self, ratio):
        count = 0
        num_neg = int(self.get_num_edges() * ratio / (1 - ratio))
        random.seed(9)
        num_vert = len(self.vdict)
        while count < num_neg:
            i = random.randint(0, num_vert - 1)
            j = random.randint(0, num_vert - 1)
            if i == j:
                continue
            i = self.return_v(i)
            j = self.return_v(j)
            print(count, i, j)
            self.add_edge(i, j, 0)
            count += 1
            print(count)
        print("The num of negative edges is: ", num_neg)

    # extract subgraph
    def sub_graph(self, vi, vj):
        gp_sub = directed_graph()
        Tvi = set(self.gp.neighbors(vi))
        Tvi.add(vi)
        Tvj = set(self.gp.neighbors(vj))
        Tvj.add(vj)
        Tvij = list(Tvi.union(Tvj))

        ls = []
        for k in range(0, len(Tvij) - 1):
            for l in range(k + 1, len(Tvij)):
                ls.append((self.return_v(Tvij[k]), self.return_v((Tvij[l]))))
        gp_sub.add_edge(ls)
        return gp_sub

    # jaccard similarity of common neighbors
    def f1_jaccard(self, i, j):
        return self.gp.similarity_jaccard(pairs=[(i, j)])[0]

    # dice similarity of common neighbors
    def f2_dice(self, i, j):
        return self.gp.similarity_dice(pairs=[(i, j)])[0]

    # cosine similarity of common neighbors
    def f3_cosine(self, i, j):

        ni = self.gp.neighbors(i)
        nj = self.gp.neighbors(j)
        uij = list(set(ni) & set(nj))
        return len(uij) / (len(ni) * len(nj))

    # recursie simrank
    def f4_simrank(self, r=0.8, iter=1, eps=1e-4):
        nodes = VertexSeq(self.gp)
        r = 0.8
        sim_prev = np.zeros(len(nodes))
        sim = np.identity(len(nodes))

        for i in range(iter):
            if np.allclose(sim, sim_prev, atol=eps):
                break
            sim_prev = np.copy(sim)
            for u, v in itertools.product(nodes, nodes):
                if u is v:
                    continue
                u = u.index
                v = v.index
                uns, vns = self.gp.neighbors(u), self.gp.neighbors(v)
                s_uv = sum([sim_prev[un][vn] for un, vn in itertools.product(uns, vns)])
                sim[u][v] = (r * s_uv) / (len(uns) * len(vns))
                print(s_uv)
                print(r)
        return sim

    # preferential attachment
    def f5_attach(self, i, j):
        ni = self.gp.neighbors(i)
        nj = self.gp.neighbors(j)
        return len(ni) * len(nj)

    # custom edge weight
    def edge_weight(self, v, direction):
        if direction == "out":
            return 1.0 / math.sqrt(1 + self.gp.outdegree([v])[0])
        elif direction == "in":
            return 1.0 / math.sqrt(1 + self.gp.indegree([v])[0])

    def f6_knn_1(self, i, j):
        i_out = self.edge_weight(i, "out")
        j_in = self.edge_weight(j, "in")
        return i_out + j_in

    def f6_knn_2(self, i, j):
        i_out = self.edge_weight(i, "out")
        j_in = self.edge_weight(j, "in")
        return i_out * j_in

    def f7_shortest(self, i, j):
        return len(self.gp.get_all_shortest_paths(i, j))

    def f8_adar(self, i, j):
        ni = self.gp.neighbors(i)
        nj = self.gp.neighbors(j)
        uij = list(set(ni) & set(nj))
        adar = 0
        for v in uij:
            nv = self.gp.neighbors(v)
            if len(nv) > 1:
                adar += 1 / math.log(len(nv))
        return adar

    def f9_maxflow(self, i, j):
        return self.gp.maxflow(i, j).value

    # calculate all the features
    def all_features(self, i, j):
        return [self.f1_jaccard(i, j), self.f2_dice(i, j), self.f3_cosine(i, j), self.f4_simrank(i, j),
                self.f5_attach(i, j),
                self.f6_knn_1(i, j), self.f6_knn_2(i, j), self.f7_shortest(i, j), self.f8_adar(i, j),
                self.f9_maxflow(i, j)]


# load data from database, randomy generate negative examples and save in static files
def load_save(g, ratio, dname, fname):
    g.load_graph(dname)
    g.random_neg(ratio)
    g.save_graph(fname)
    print("number of edges:", len(g.edge_list()))


def build_classifier(g, graph_name):
    g.read_graph(graph_name)
    x = []
    y = []
    # load graph data
    for pair in g.edge_list():
        x.append(g.all_features(pair.tuple[0], pair.tuple[1]))
        y.append(pair["weight"])
    print("load finish")
    # split into test set and train set
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
    print("split finish")
    time2 = time.time()
    print("training length:", len(y_train))
    print("building classifier")
    # build classifier
    clf = RandomForestClassifier(n_estimators=500, max_depth=10, min_samples_leaf=54, max_features="auto",
                                 oob_score=True)
    # train the classifir
    clf.fit(x_train, y_train)
    time3 = time.time()
    print("classifier built", time3 - time2)
    # make predicition
    predicts = clf.predict(x_test)
    print("Accuracy: {:.2%}".format(accuracy_score(y_test, predicts)))
    # 10 fold validation score
    scores = cross_val_score(clf, x, y, cv=10)
    print("Overall accuracy:", scores)
    filename = 'classifier_rf.joblib.pkl'
    # save the classifier
    joblib.dump(clf, filename, compress=1)


if __name__ == '__main__':
    # name of the graph file to be saved
    fname = "graph"
    # name of the classifier file to be saved
    classifer_name = "clf"
    # ratio of postive and negative
    ratio = 0.5
    g = directed_graph()
    # name of database
    dname = "filtered_network3_rep"
    g.read_graph("test")
    load_save(g, ratio, dname, fname)
    build_classifier(g, classifer_name)
