from django.shortcuts import render

# Create your views here.
# !/usr/bin/env python3
# -*- coding:UTF-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import json
import requests
from django.views.decorators.csrf import csrf_exempt
from .predict_friend import predict
from .forms import NameForm
from .directed_graph import directed_graph
from sklearn.externals import joblib

# Create your views here.

# website setup 1. load graph 2.load classifier
graph = directed_graph()
print("reading graph")
graph.read_graph("dcpj_app/data/mid")
print(len(graph.edge_list()))
print("reading classifier")
clf = joblib.load('dcpj_app/data/classifier_rf.joblib.pkl')
print("success!")


def main(request):
    return render(request, 'main.html')


def popularcenter(request):
    return render(request, 'pcenter.html')


def centeractivity(request):
    all_list = []
    fp = open("dcpj_app/static/data/cactivities.txt")
    for line in fp.readlines():
        single_line = line.rstrip("\n").replace('"', "").split(" ")
        all_list.append(single_line)
    print(all_list)
    return render(request, 'cactivity.html', {"js_data": all_list})


def popularuser(request):
    fp = open("dcpj_app/static/data/top10.json")
    fp_json = json.load(fp)
    print("Parsed json:", fp_json)
    return render(request, 'puser.html', {"sub_graph": fp_json})


def networkgraph(request):
    return render(request, 'network.html')


@csrf_exempt
def ml(request):
    return render(request, 'ml.html')


@csrf_exempt
def get_username(request):
    if request.method == 'GET':
        a = request.GET.get('username', '')
        if a:
            print(a)
            result = predict(a, graph, clf)
            return HttpResponse(result)
    return HttpResponse()
