from django.apps import AppConfig


class DcpjAppConfig(AppConfig):
    name = 'dcpj_app'
