from django.conf.urls import url
from django.contrib import admin
from dcpj_app import views as view
urlpatterns = [
    url(r'^predict/tweeter/', view.get_username),

    url(r'^pcenters/', view.popularcenter),
    url(r'^cactivity/', view.centeractivity),
    url(r'^pusers/', view.popularuser),
    url(r'^network/', view.networkgraph),
    url(r'^predict/', view.ml),
    url(r'^$', view.main),

]
