from sklearn.externals import joblib
from dcpj_app.auth_token import consumer_key, consumer_secret, access_token, access_secret
from dcpj_app.directed_graph import directed_graph
import tweepy
import numpy as np
import json
from dcpj_app.centers import center
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)


# predict potential friends
def predict(username, graph, clf):
    result = {"friends": [], "status": ""}
    try:
        user_id = api.get_user(screen_name=username).id
    except tweepy.error.TweepError:
        result["status"] = "no_user"
        return json.dumps(result)
    # if the id is in the graph...
    if user_id in graph.vdict:
        print("we have it!")
        print(user_id)
        result["status"] = "yes"
        num = len(graph.vdict)
        index = graph.vdict[user_id]
        neighbors = graph.get_neighbors(index)
        print(neighbors)
        for i in range(num):
            fea = np.array(graph.all_features(index, i)).reshape(1, -1)
            if clf.predict(fea) == 1:
                if (i not in neighbors) & (graph.return_v(i) not in center) & (i != index):
                    predict_id = graph.return_v(i)
                    try:
                        pre_name = api.get_user(id=predict_id).screen_name
                        print(pre_name)
                        result["friends"].append(pre_name)
                    except:
                        pass
                    if len(result["friends"]) == 20:
                        break
        print(result)
        return json.dumps(result)
    else:
        # not in igraph
        result["status"] = "no_db"
        return json.dumps(result)

if __name__ == "__main__":
    # test
    if predict("Finola_Finn"):
        print("got!")
