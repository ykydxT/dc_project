# harvest activities of Twitter accounts
# not used in the project, because API can only return up to 3,200 of an account’s most recent

from db_connection import couchdb_connect
from auth_token import consumer_key, consumer_secret, access_token, access_secret
import tweepy
import json


def get_timeline():
    db = couchdb_connect("cactivity")
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    fp = open("center_tweetid.json", "r")
    tweets_id = json.load(fp)
    count = 0
    all_status = []
    for tweet in tweets_id:
        status = api.get_status(tweet)
        status_json = status._json
        all_status.append(status_json)
        count += 1
        print(count)
    db.update(all_status)
    print(len(all_status))


