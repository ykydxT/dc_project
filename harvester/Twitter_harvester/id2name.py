# convert id to username
import tweepy
from auth_token import consumer_key, consumer_secret, access_token, access_secret
import re
import json
import time
import couchdb
from db_pwd import dbuser, password
from db_connection import couchdb_connect
import sys


def id2name():
    auth = tweepy.OAuthHandler(consumer_key,consumer_secret)
    auth.set_access_token(access_token,access_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    db =couchdb_connect("l1user")
    db2 = couchdb_connect("l1user_name")
    db2_records = []
    count = 0

    for row in db.iterview('_all_docs', 10000, include_docs=True):
        count += 1
        print(count, row.doc["user_id"])
        try:
            name = api.get_user(row.doc["user_id"]).screen_name
            record = {"username": name, "_id": name, "uid": row.doc["user_id"]}
            db2.save(record)
        except:
            pass
            print(row.doc["user_id"])

if __name__ == '__main__':
    id2name()