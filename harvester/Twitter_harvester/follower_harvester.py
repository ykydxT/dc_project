# harvest the followers of official accounts in UniMelb
import tweepy
from auth_token import consumer_key, consumer_secret, access_token, access_secret
import re
import json
import time
import couchdb
from db_pwd import dbuser, password
from db_connection import couchdb_connect


class user():
    def __init__(self, screen_name, description):
        self.screen_name = screen_name
        self.description = description

    def is_name(self, patterns):
        for pattern in patterns:
            if re.search(pattern, self.screen_name, re.IGNORECASE) or re.search(pattern, self.description, re.IGNORECASE):
                return True
        return False

# divide a list into several chunks
def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]

if __name__ == '__main__':
    auth = tweepy.OAuthHandler(consumer_key,consumer_secret)
    auth.set_access_token(access_token,access_secret)
    api = tweepy.API(auth)
    all_name = []
    f = open("items.json", 'r')
    all_json = json.load(f)
    for name in all_json:
        all_name.append(name["screen_name"])
    name_chunks = list(chunks(all_name, 100))
    right_account = []
    for chunk in name_chunks:
        accounts = api.lookup_users(screen_names=chunk)
        for i in accounts:
            a = user(i.screen_name, i.description)
            # filter the accounts that are not necessarily related to UniMelb
            if a.is_name(["unimelb", "uom", "university of melbourne", "melbourne uni"]):
                right_account.append(a.screen_name)
    print(len(right_account), right_account)

    cdb = couchdb_connect("l1user_new")
    ids = []
    count = 0
    for account in right_account:
        count += 1
        if count > 15:
            print("waiting...")
            time.sleep(60*15)
            count = 0
        for page in tweepy.Cursor(api.followers_ids, screen_name=account).pages():
            for user_id in page:
                # links from followers to centers are stored in database
                user_doc = {"user_id": user_id, "_id": str(user_id) + str(account), "center": account}
                try:
                    cdb.save(user_doc)
                except couchdb.http.ResourceConflict:
                    print("user repeat")
                except:
                    print("save error!!!")

            ids.extend(page)
            print(count, len(page), len(ids))
            print()
            count += 1
            if count > 15:
                count = 0
                time.sleep(60*15)
    print(len(ids))

