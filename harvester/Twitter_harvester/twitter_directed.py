# harvest links among followers, 15 tokens are used
import tweepy
import time
from auth_token import tokenss
from db_connection import couchdb_connect

# check if 15 tokens all been used
def judge():
    global index, index_token, lens, api, count_num
    index_token += 1
    if index_token == 15:
        index_token = 0
        index += 1

        if index == lens:
            print("one more round")
            index = 0
        else:
            print("newtoken", index)

        api = new_api(index)


# store the links into database
def store_page(page, from_id, dbname, users):
    db = couchdb_connect(dbname)
    records = []
    for to_id in page:
        if str(to_id) in users:
            id = str(from_id) + str(to_id)
            records.append({"_id": id,  "to": from_id, "from": to_id})
    db.update(records)


# change the token and generate a new api
def new_api(index):
    new_token = tokenss[int(index)]
    print(new_token.access_secret)
    auth = tweepy.OAuthHandler(new_token.consumer_key, new_token.consumer_secret)
    auth.set_access_token(new_token.access_token, new_token.access_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    return api

if __name__ == '__main__':
    lens = len(tokenss)
    print("number of tokens:", lens)
    cdb = couchdb_connect("l1user")
    count = 0
    index = 0
    api = new_api(index)
    # global token index
    index_token = 0
    count_num = 0
    flagg = 1
    users = []
    for row in cdb.view('_all_docs'):
         users.append(row.id)
    for row in cdb.view('_all_docs'):
        count_num += 1
        if flagg == 1:
            print(count_num)
            try:
                for page in tweepy.Cursor(api.followers_ids, id=row.id).pages():
                    print(row.id)
                    store_page(page, int(row.id), "filtered_network2", users)
                    judge()
            except tweepy.TweepError as error:
                # some accounts are protected by security policy
                if str(error) == 'Not authorized.':
                    print('Not authorized')
                    judge()
                    pass
                elif str(error) == 'Over capacity':
                    time.sleep(60*15)
                    print(str(error))
                else:
                    print("unexpected", error)
                    pass


