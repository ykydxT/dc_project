# scrape orgnization accounts in UniMelb
import scrapy
from unimelb_crawler.items import UnimelbItem
import re

class UnimelbSpider(scrapy.Spider):
    name = "unimelb"
    allowed_domains = ["unimelb.edu.au"]
    start_urls = [
        "https://socialmedia.unimelb.edu.au/directory?q=&network%5B%5D=Twitter"
    ]

    def parse(self, response):
        for sel in response.selector.xpath("//a[@class='button']").re(r'https:\/\/twitter.com\/(\w+)'):

            item = UnimelbItem()
            item["screen_name"] = sel
            yield item
        next_page = response.xpath("//span[@class='next']//a[.='Next ›']/@href").extract()
        if next_page:
            next_href = next_page[0]
            next_page_url = 'https://socialmedia.unimelb.edu.au' + next_href
            request = scrapy.Request(url=next_page_url)
            yield request
        filename = response.url.split("/")[-2]
        with open(filename, 'wb') as f:
                f.write(response.body)

