# generate network data(follower or being followed) of top 10 popular users

from db_connection import couchdb_connect
import operator
import json

# convert id to username
def idtouser():
    id2user = {}
    db1 = couchdb_connect("l1user_name")
    for row in db1.iterview('_all_docs', 50000, include_docs=True):
        id = row.doc["uid"]
        name = row.doc["username"]
        id2user[id] = name
    return id2user

if __name__ == '__main__':
    id2user = idtouser()
    top_10 = [{"name": "martinditmann", "follower":[], "following":[]},
              {"name": "tim_lynchphd", "follower":[], "following":[]},
              {"name": "katieaewing", "follower":[], "following":[]},
              {"name": "Hawkins_Kevin", "follower":[], "following":[]},
              {"name": "MikeRonchi", "follower":[], "following":[]},
              {"name": "jillebenn", "follower":[], "following":[]},
              {"name": "NadiRhook", "follower":[], "following":[]},
              {"name": "PeterGGahan", "follower":[], "following":[]},
              {"name": "simmodross", "follower":[], "following":[]},
              {"name": "KateCCranney", "follower":[], "following":[]}]
    db2 = couchdb_connect("filtered_network3_rep")
    all_user = {}
    count =1
    for row in db2.iterview('_all_docs', 50000, include_docs=True):
        count+=1
        try:
            ufrom = str(row.doc["from"])
            uto = str(row.doc["to"])
        except:
            ufrom = row.doc["user_id"]
            uto = row.doc["center"]
        if ufrom in id2user:
             ufrom = id2user[ufrom]
        if uto in id2user:
            uto = id2user[uto]
        for each_top in top_10:
            if each_top["name"] == ufrom:
                print("get a following", each_top["name"])
                each_top["following"].append(uto)
                break
            elif each_top["name"] == uto:
                print("get a follower", each_top["name"])
                each_top["follower"].append(ufrom)
                break
    print(top_10)
    with open("top10.json","w") as f:
        json.dump(top_10, f)

