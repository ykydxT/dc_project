# generate timeline file, used in web server

from db_connection import couchdb_connect
from dateutil import parser
from operator import itemgetter


# modify format of time
def time_format(original):
    times = original.split(" ")
    day = times[2]
    month = times[1]
    year = times[-1]
    return year + "-" + month + "-" + day

if __name__ == '__main__':
    db = couchdb_connect("cactivity")
    all_time = {}
    for row in db.iterview('_all_docs', 10000, include_docs=True):
        screen_name = row.doc["user"]["screen_name"]
        # timestamp of tweets
        ctime = time_format(row.doc["created_at"])
        if screen_name not in all_time:
            all_time[screen_name] = [[ctime, 1]]
        else:
            # count the number of tweets on one single day
            for i in range(0, len(all_time[screen_name])):
                if all_time[screen_name][i][0] == ctime:
                    all_time[screen_name][i][1] += 1
                    break
                elif i == len(all_time[screen_name]) - 1:
                    all_time[screen_name].append([ctime, 1])
                    break

    fp = open("cactivities.txt", "w")
    all_list = []
    for j in all_time:
        for k in all_time[j]:
            all_list.append([j, parser.parse(k[0]), k[1]])
    # sort the timelines by dates
    all_list = sorted(all_list, key=itemgetter(1))
    print(all_list)
    for slist in all_list:
            dtime = str(slist[1]).split(" ")[0]
            st = slist[0] + " " + dtime + " " + str(slist[2]) + "\n"
            fp.write(st)

