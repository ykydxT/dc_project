# generate complete network (include links between followers)data of top 10 popular users
from directed_graph import directed_graph
import csv
import json
from topten import top_ten, top_ten_id, top_three
from db_connection import couchdb_connect
from follower import idtouser


def sub_graph(fname):
    with open(fname) as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            if row[1] in top_ten:
                print(row)


def whole_graph(fname):
    id2user = idtouser()
    f = open(fname, "w")
    spamwriter = csv.writer(f, dialect='excel')
    spamwriter.writerow(["source", "target"])
    graph = directed_graph()
    print("reading graph")
    graph.read_graph("data/test")
    es = graph.edge_list()
    count = 0
    for i in es:
        count += 1
        print(count)
        if i["weight"] == 1.0:
            from_n = str(graph.return_v(i.tuple[0]))
            to_n = str(graph.return_v(i.tuple[1]))
            if from_n in id2user:
                from_n = id2user[from_n]
            if to_n in id2user:
                to_n = id2user[to_n]
            row = [from_n, to_n]
            print(row)
            spamwriter.writerow(row)

if __name__ == '__main__':
    lt = []
    with open("top10.json", "r") as f:
        aaa =json.load(f)
        num = 0
        for i in aaa:
            num += 1
            if num>3:
                break
            for j in i["follower"]:
                if j not in lt:
                    lt.append(j)
    print(len(lt))
    f = open("subgraph.csv", "w")
    spamwriter = csv.writer(f, dialect='excel')
    spamwriter.writerow(["source", "target"])
    db2 = couchdb_connect("filtered_network3_rep")
    iddd = idtouser()
    print("ww")
    count = 0
    for row in db2.iterview('_all_docs', 50000, include_docs=True):
        try:
            from_u = str(row.doc["from"])
            to_u = str(row.doc["to"])
            from_u = iddd[from_u]
            to_u = iddd[to_u]
            count += 1
            if (from_u in lt) & (to_u in lt):
                roww = [from_u, to_u]
                print(count,from_u, to_u)
                spamwriter.writerow(roww)
            elif to_u in top_three:
                roww = [from_u, to_u]
                print(count, from_u, to_u)
                spamwriter.writerow(roww)
        except:
            pass






